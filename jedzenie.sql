-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 15 Gru 2018, 12:31
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Baza danych: `mta_jedzenie`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galerie_zdjecia`
--

DROP TABLE IF EXISTS `galerie_zdjecia`;
CREATE TABLE `galerie_zdjecia` (
  `id_galeria` int(11) UNSIGNED NOT NULL,
  `id_zdjecie` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gelerie`
--

DROP TABLE IF EXISTS `gelerie`;
CREATE TABLE `gelerie` (
  `id_galeria` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(40) NOT NULL,
  `data_utworzenia` date NOT NULL,
  `data_aktualizacji` date DEFAULT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

DROP TABLE IF EXISTS `kategorie`;
CREATE TABLE `kategorie` (
  `id_kategoria` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL COMMENT 'To pole przechowuje odniesienie do opiekuna całej kategorii',
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `komentarze`
--

DROP TABLE IF EXISTS `komentarze`;
CREATE TABLE `komentarze` (
  `id_komentarz` int(11) UNSIGNED NOT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL COMMENT 'Osoba dodająca komentarz',
  `data` date NOT NULL,
  `tresc` text NOT NULL,
  `id_moderator` int(11) UNSIGNED NOT NULL COMMENT 'Osoba moderująca wiadomość',
  `data_aktualizacji` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` int(11) UNSIGNED NOT NULL,
  `imie` varchar(35) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `urodziny` date NOT NULL,
  `haslo` char(31) NOT NULL,
  `e_mail` varchar(100) NOT NULL,
  `plec` tinyint(1) NOT NULL,
  `id_ranga` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `nick`, `urodziny`, `haslo`, `e_mail`, `plec`, `id_ranga`) VALUES
(1, 'Justyna', 'Justynowska', 'justys', '1987-05-11', 'ea46d6da2e2289d0504a0c2316c13cf', 'justys@gmail.com', 1, 1),
(2, 'Marcin', 'Marcinowski', 'maciek', '1960-11-16', 'ecdfb0fe8b11f4844d25ef09988e494', 'jakis_adres@op.pl', 0, 2),
(3, 'Marta', 'Martowska', 'marti', '1960-11-16', 'ecdfb0fe8b11f4844d25ef09988e494', 'jakis_adres2@op.pl', 1, 2),
(4, 'Michalina', 'Michalinowska', 'misha', '1960-11-16', 'ecdfb0fe8b11f4844d25ef09988e494', 'jakis_adres3@op.pl', 1, 3),
(6, 'Grażyna', 'Januszowska', 'halyna', '1960-11-16', 'ecdfb0fe8b11f4844d25ef09988e494', 'jakis_adres5@op.pl', 1, 3),
(7, 'Sebastian', 'Sebastianowski', 'sebix', '1978-11-16', 'ecdfb0fe8b11f4844d25ef09988e494', 'sebix@op.pl', 0, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy`
--

DROP TABLE IF EXISTS `przepisy`;
CREATE TABLE `przepisy` (
  `id_przepis` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `id_kategoria` int(11) UNSIGNED NOT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL,
  `data_dodania` date NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_komentarze`
--

DROP TABLE IF EXISTS `przepisy_komentarze`;
CREATE TABLE `przepisy_komentarze` (
  `id_przepis` int(11) UNSIGNED NOT NULL,
  `id_komentarz` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_skladniki`
--

DROP TABLE IF EXISTS `przepisy_skladniki`;
CREATE TABLE `przepisy_skladniki` (
  `id_przepis` int(11) UNSIGNED NOT NULL,
  `id_skladnik` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rangi`
--

DROP TABLE IF EXISTS `rangi`;
CREATE TABLE `rangi` (
  `id_ranga` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `opis` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `rangi`
--

INSERT INTO `rangi` (`id_ranga`, `nazwa`, `opis`) VALUES
(1, 'Administrator', 'Może wszystko'),
(2, 'Moderator', 'Może mniej'),
(3, 'Użytkownik', 'Nie może nic');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `role_uzytkownik`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `role_uzytkownik`;
CREATE TABLE `role_uzytkownik` (
`Imię` varchar(35)
,`Nazwisko` varchar(50)
,`Data urodzin` date
,`Pełniona funkcja` varchar(20)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `skladniki`
--

DROP TABLE IF EXISTS `skladniki`;
CREATE TABLE `skladniki` (
  `id_skladnik` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `jednostka_miary` tinyint(1) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `strony`
--

DROP TABLE IF EXISTS `strony`;
CREATE TABLE `strony` (
  `id_strona` int(11) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `tresc` text NOT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL,
  `data_aktualizacji` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecia`
--

DROP TABLE IF EXISTS `zdjecia`;
CREATE TABLE `zdjecia` (
  `id_zdjecie` int(11) UNSIGNED NOT NULL,
  `zdjecie` longblob NOT NULL,
  `opis` text NOT NULL,
  `id_osoba` int(11) UNSIGNED NOT NULL,
  `data_dodania` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura widoku `role_uzytkownik`
--
DROP TABLE IF EXISTS `role_uzytkownik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `role_uzytkownik`  AS  select `osoby`.`imie` AS `Imię`,`osoby`.`nazwisko` AS `Nazwisko`,`osoby`.`urodziny` AS `Data urodzin`,`rangi`.`nazwa` AS `Pełniona funkcja` from (`osoby` join `rangi` on((`osoby`.`id_ranga` = `rangi`.`id_ranga`))) ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `galerie_zdjecia`
--
ALTER TABLE `galerie_zdjecia`
  ADD KEY `id_galeria` (`id_galeria`),
  ADD KEY `id_zdjecie` (`id_zdjecie`);

--
-- Indexes for table `gelerie`
--
ALTER TABLE `gelerie`
  ADD PRIMARY KEY (`id_galeria`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id_kategoria`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `komentarze`
--
ALTER TABLE `komentarze`
  ADD PRIMARY KEY (`id_komentarz`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_moderator` (`id_moderator`);

--
-- Indexes for table `osoby`
--
ALTER TABLE `osoby`
  ADD PRIMARY KEY (`id_osoba`),
  ADD KEY `id_ranga` (`id_ranga`);

--
-- Indexes for table `przepisy`
--
ALTER TABLE `przepisy`
  ADD PRIMARY KEY (`id_przepis`),
  ADD KEY `id_kategoria` (`id_kategoria`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `przepisy_komentarze`
--
ALTER TABLE `przepisy_komentarze`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_komentarz` (`id_komentarz`);

--
-- Indexes for table `przepisy_skladniki`
--
ALTER TABLE `przepisy_skladniki`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_skladnik` (`id_skladnik`);

--
-- Indexes for table `rangi`
--
ALTER TABLE `rangi`
  ADD PRIMARY KEY (`id_ranga`);

--
-- Indexes for table `skladniki`
--
ALTER TABLE `skladniki`
  ADD PRIMARY KEY (`id_skladnik`);

--
-- Indexes for table `strony`
--
ALTER TABLE `strony`
  ADD PRIMARY KEY (`id_strona`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indexes for table `zdjecia`
--
ALTER TABLE `zdjecia`
  ADD PRIMARY KEY (`id_zdjecie`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `gelerie`
--
ALTER TABLE `gelerie`
  MODIFY `id_galeria` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  MODIFY `id_kategoria` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  MODIFY `id_komentarz` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  MODIFY `id_przepis` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `rangi`
--
ALTER TABLE `rangi`
  MODIFY `id_ranga` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `skladniki`
--
ALTER TABLE `skladniki`
  MODIFY `id_skladnik` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `strony`
--
ALTER TABLE `strony`
  MODIFY `id_strona` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  MODIFY `id_zdjecie` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `galerie_zdjecia`
--
ALTER TABLE `galerie_zdjecia`
  ADD CONSTRAINT `galerie_zdjecia_ibfk_1` FOREIGN KEY (`id_galeria`) REFERENCES `gelerie` (`id_galeria`),
  ADD CONSTRAINT `galerie_zdjecia_ibfk_2` FOREIGN KEY (`id_zdjecie`) REFERENCES `zdjecia` (`id_zdjecie`);

--
-- Ograniczenia dla tabeli `gelerie`
--
ALTER TABLE `gelerie`
  ADD CONSTRAINT `gelerie_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `kategorie`
--
ALTER TABLE `kategorie`
  ADD CONSTRAINT `kategorie_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `komentarze`
--
ALTER TABLE `komentarze`
  ADD CONSTRAINT `komentarze_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `komentarze_ibfk_2` FOREIGN KEY (`id_moderator`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_ranga`) REFERENCES `rangi` (`id_ranga`);

--
-- Ograniczenia dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  ADD CONSTRAINT `przepisy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`),
  ADD CONSTRAINT `przepisy_ibfk_2` FOREIGN KEY (`id_kategoria`) REFERENCES `kategorie` (`id_kategoria`);

--
-- Ograniczenia dla tabeli `przepisy_komentarze`
--
ALTER TABLE `przepisy_komentarze`
  ADD CONSTRAINT `przepisy_komentarze_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `przepisy_komentarze_ibfk_2` FOREIGN KEY (`id_komentarz`) REFERENCES `komentarze` (`id_komentarz`);

--
-- Ograniczenia dla tabeli `przepisy_skladniki`
--
ALTER TABLE `przepisy_skladniki`
  ADD CONSTRAINT `przepisy_skladniki_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `przepisy_skladniki_ibfk_2` FOREIGN KEY (`id_skladnik`) REFERENCES `skladniki` (`id_skladnik`);

--
-- Ograniczenia dla tabeli `strony`
--
ALTER TABLE `strony`
  ADD CONSTRAINT `strony_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  ADD CONSTRAINT `zdjecia_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);
COMMIT;
